use xinabo;
INSERT INTO 
	tipo_solicitacoes(titulo_solicitacao, descricao)
VALUES
	('Entulhos','Coleta de entulhos em pequena quantidade, limitada a 5 carrinhos de mão.'),
	('Calicas','Coleta de restos de construção, limitada a 5 carrinhos de mão.'),
	('Lixo Vegetal','Coleta de lixos vegetais, restos de gramas, podas de árvore, limitada a 10 carrinhos de mão.')