CREATE database xinabo;
use xinabo;
CREATE TABLE usuario (
    id int NOT NULL AUTO_INCREMENT,
    nome varchar(50) NOT NULL,
    cpf varchar(14) NOT NULL,
    email varchar(50) NOT NULL,
    senha varchar(50) NOT NULL,
    dark_mode boolean DEFAULT false,
    flg_ativo boolean DEFAULT true,
    flg_admin boolean DEFAULT false,
    PRIMARY KEY (id)
);
CREATE TABLE tipo_solicitacoes (
    id_solicitacao int NOT NULL AUTO_INCREMENT,
    titulo_solicitacao varchar(50) NOT NULL,
    descricao varchar(200) NOT NULL,
    PRIMARY KEY (id_solicitacao)
);

CREATE TABLE coleta (
    id_coleta int NOT NULL AUTO_INCREMENT,
    bairro varchar(50) NOT NULL,
    tipo_coleta varchar(50) NOT NULL,
    horario time NOT NULL,
    dia_semana varchar(20) NOT NULL,
    PRIMARY KEY (id_coleta)
);

CREATE TABLE solicitacoes (
    cod_solicitacao int NOT NULL AUTO_INCREMENT,
    id_usuario int NOT NULL,
    tipo_solicitacao int NOT NULL,
    data_cadastro date NOT NULL,
    data_agendamento date NOT NULL,
    comentario varchar(200) NOT NULL,
    endereco varchar(100) NOT NULL, 
    flg_ativo boolean DEFAULT true,
    PRIMARY KEY (cod_solicitacao),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    FOREIGN KEY (tipo_solicitacao) REFERENCES tipo_solicitacoes(id_solicitacao)
);


CREATE TABLE lembretes (
    cod_lembrete int NOT NULL AUTO_INCREMENT,
    id_usuario int NOT NULl,
    data_lembrete date NOT NULL,
    descricao varchar(200) NOT NULL,
    flg_ativo boolean DEFAULT true,
    PRIMARY KEY (cod_lembrete),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);

INSERT INTO usuario (nome, cpf, email, senha, dark_mode, flg_ativo, flg_admin) VALUES
('Teste', '11111111111', 'teste@gmail.com', 'e8d95a51f3af4a3b134bf6bb680a213a', true, true, false),
('Admin', '11111111111', 'admin@gmail.com', 'e8d95a51f3af4a3b134bf6bb680a213a', true, true, true);
