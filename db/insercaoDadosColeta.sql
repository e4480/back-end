use xinabo;
INSERT INTO 
	coleta(bairro, tipo_coleta, horario, dia_semana)
VALUES
	
	('Alto Boqueirao','Reciclavel','183000', 'segunda'),
	('Alto Boqueirao','Reciclavel','183000', 'sexta'),
	('Alto Boqueirao','Lixo comum','200000', 'terca'),
    ('Alto Boqueirao','Lixo comum','200000', 'quinta'),
    
    ('Xaxim','Reciclavel','203000', 'segunda'),
	('Xaxim','Reciclavel','203000', 'sexta'),
	('Xaxim','Lixo comum','210000', 'terca'),
    ('Xaxim','Lixo comum','210000', 'quinta'),
    
    ('Vila Izabel','Reciclavel','150000', 'terca'),
	('Vila Izabel','Reciclavel','150000', 'sexta'),
	('Vila Izabel','Lixo comum','210000', 'segunda'),
    ('Vila Izabel','Lixo comum','210000', 'quinta'),
    
    ('Umbara','Reciclavel','190000', 'quarta'),
	('Umbara','Reciclavel','190000', 'sexta'),
	('Umbara','Lixo comum','200000', 'segunda'),
    ('Umbara','Lixo comum','200000', 'terca'),
    
    ('Tingui','Reciclavel','200000', 'segunda'),
	('Tingui','Reciclavel','200000', 'sexta'),
	('Tingui','Lixo comum','220000', 'terca'),
    ('Tingui','Lixo comum','220000','quinta'),
    
    ('Tatuquara','Reciclavel','180000', 'quarta'),
	('Tatuquara','Reciclavel','180000', 'quinta'),
	('Tatuquara','Lixo comum','200000', 'segunda'),
    ('Tatuquara','Lixo comum','200000', 'sexta'),
    
    ('Taruma','Reciclavel','150000', 'segunda'),
	('Taruma','Reciclavel','150000', 'sexta'),
	('Taruma','Lixo comum','210000', 'terca'),
    ('Taruma','Lixo comum','210000', 'quinta'),
    
    ('Taboao','Reciclavel','170000', 'quinta'),
	('Taboao','Reciclavel','170000', 'sexta'),
	('Taboao','Lixo comum','220000', 'segunda'),
    ('Taboao','Lixo comum','220000', 'quinta'),
    
    
    ('Santo Inacio','Reciclavel','200000', 'segunda'),
	('Santo Inacio','Reciclavel','200000', 'quarta'),
	('Santo Inacio','Lixo comum','210000', 'terca'),
    ('Santo Inacio','Lixo comum','210000', 'sexta'),
    
    ('Reboucas','Reciclavel','183000', 'segunda'),
	('Reboucas','Reciclavel','183000', 'sexta'),
	('Reboucas','Lixo comum','213000', 'terca'),
    ('Reboucas','Lixo comum','213000', 'quinta'),
    
    ('Prado Velho','Reciclavel','170000', 'quarta'),
	('Prado Velho','Reciclavel','170000', 'sexta'),
	('Prado Velho','Lixo comum','180000', 'segunda'),
    ('Prado Velho','Lixo comum','180000', 'quinta'),
    
    ('Pinheirinho','Reciclavel','090000', 'terca'),
	('Pinheirinho','Reciclavel','090000', 'quinta'),
	('Pinheirinho','Lixo comum','083000', 'segunda'),
    ('Pinheirinho','Lixo comum','083000', 'sexta'),
    
    ('Parolin','Reciclavel','093000', 'segunda'),
	('Parolin','Reciclavel','093000', 'sexta'),
	('Parolin','Lixo comum','210000', 'terca'),
    ('Parolin','Lixo comum','210000', 'quinta'),
    
    ('Novo Mundo','Reciclavel','190000', 'quarta'),
	('Novo Mundo','Reciclavel','190000', 'quinta'),
	('Novo Mundo','Lixo comum','080000', 'segunda'),
    ('Novo Mundo','Lixo comum','080000', 'sexta'),
    
    ('Lindoia','Reciclavel','183000', 'segunda'),
	('Lindoia','Reciclavel','183000', 'sexta'),
	('Lindoia','Lixo comum','213000', 'terca'),
    ('Lindoia','Lixo comum','213000', 'quinta'),
    
    ('Juveve','Reciclavel','090000', 'terca'),
	('Juveve','Reciclavel','090000', 'quinta'),
	('Juveve','Lixo comum','200000', 'segunda'),
    ('Juveve','Lixo comum','200000', 'sexta'),
    
    ('Jardim Social','Reciclavel','083000', 'quarta'),
	('Jardim Social','Reciclavel','083000', 'sexta'),
	('Jardim Social','Lixo comum','180000', 'terca'),
    ('Jardim Social','Lixo comum','180000', 'quinta'),
    
    ('Hugo Lange','Reciclavel','213000', 'segunda'),
	('Hugo Lange','Reciclavel','213000', 'sexta'),
	('Hugo Lange','Lixo comum','090000', 'terca'),
    ('Hugo Lange','Lixo comum','090000', 'quinta'),
    
    ('Guaira','Reciclavel','183000', 'segunda'),
	('Guaira','Reciclavel','183000', 'quarta'),
	('Guaira','Lixo comum','190000', 'quinta'),
    ('Guaira','Lixo comum','190000', 'terca'),
    
    ('Lamenha Pequena ','Reciclavel','180000', 'segunda'),
	('Lamenha Pequena ','Reciclavel','180000', 'sexta'),
	('Lamenha Pequena ','Lixo comum','200000', 'terca'),
    ('Lamenha Pequena ','Lixo comum','200000', 'quarta'),
    
    ('Jardim Botanico','Reciclavel','080000', 'terca'),
	('Jardim Botanico','Reciclavel','080000', 'sexta'),
	('Jardim Botanico','Lixo comum','190000', 'segunda'),
    ('Jardim Botanico','Lixo comum','190000', 'quarta'),
    
    ('Centro','Reciclavel','090000', 'terca'),
	('Centro','Reciclavel','090000', 'quinta'),
	('Centro','Lixo comum','200000', 'segunda'),
    ('Centro','Lixo comum','200000', 'sexta'),
    
    ('Campo de Santana','Reciclavel','210000', 'segunda'),
	('Campo de Santana','Reciclavel','210000', 'quarta'),
	('Campo de Santana','Lixo comum','090000', 'terca'),
    ('Campo de Santana','Lixo comum','090000', 'sexta'),
    
    ('Campina do Siqueira','Reciclavel','190000', 'terca'),
	('Campina do Siqueira','Reciclavel','190000', 'quarta'),
	('Campina do Siqueira','Lixo comum','183000', 'segunda'),
    ('Campina do Siqueira','Lixo comum','183000', 'sexta'),
    
    ('Cachoeira','Reciclavel','193000', 'segunda'),
	('Cachoeira','Reciclavel','193000', 'quinta'),
	('Cachoeira','Lixo comum','210000', 'terca'),
    ('Cachoeira','Lixo comum','210000', 'sexta') 
    
