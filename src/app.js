const express = require("express");
const cors = require("cors")
const app = express();


const bodyParser = require("body-parser");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger/swagger_output.json');

app.use(bodyParser.urlencoded({ extended: false })); //apenas dados simples
app.use(bodyParser.json()); //json de entrada no body

app.use(cors());

const solicitacoesRouter = require('./routes/solicitacoes')
const coletasRouter = require('./routes/coletas')
const usersRouter = require('./routes/users')

app.use('/coleta', coletasRouter);
app.use('/solicitacoes', solicitacoesRouter);
app.use('/users', usersRouter);
app.use('/doc', swaggerUi.serve);
app.get('/doc', swaggerUi.setup(swaggerDocument));

app.use((req, res, next) =>{
    const erro = new Error('Rota não encontrada')
    erro.status(404);
    next(erro)
});

app.use((error, req, res, next) =>{
    res.status(error.status  || 500);
    return res.send({
        error:{
            menssage: error.message
        },
        message : "Ocorreu um erro. Tente novamente mais tarde.",
        success: false
    })
})

module.exports = app;
