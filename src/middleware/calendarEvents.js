const { google } = require('googleapis');

const { OAuth2 } = google.auth

const oAuth2Client = new OAuth2('521274477402-4982vucocerifkknn7s6bl0ud9clh2gj.apps.googleusercontent.com', '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')

oAuth2Client.setCredentials({ refresh_token: '1//04rJqXXYvRKH1CgYIARAAGAQSNwF-L9IrJG9dmUI63yrnrCSrC5SXULqdhc7It_RozEHIIX8Ia0-ROtv9I_jHmFh42_7iUVn_r9c' })

const calendar = google.calendar({ version: 'v3', auth: oAuth2Client })


exports.criarEvento = (eventDate) => {
    const event = {
        summary: 'Lembre Coleta de Lixo',
        location: 'Bairro',
        description: 'Coleta de lixo reciclável',
        start: {
            date: eventDate,

        },
        end: {
            date: eventDate,

        },
        ColorId: 1
    }

    calendar.events.insert(
        { calendarId: 'primary', resource: event },
        err => {
            if (err) {
                console.log(err)
                return false
            }
            return true
        }
    )
};
