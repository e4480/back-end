const jwt = require("jsonwebtoken");

exports.obrigatorio = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    console.log(token);
    const decode = jwt.verify(token, `SeedProvisoria`);
    req.usuario = decode;
    next();
  } catch (error) {
    return res.status(401).send({ mensagem: "Falha na Autenticação" });
  }
};

exports.admin = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    console.log(token);
    const decode = jwt.verify(token, `SeedAdminProvisoria`);
    req.usuario = decode;
    next();
  } catch (error) {
    return res.status(401).send({ mensagem: "Falha na Autenticação" });
  }
};
