const express = require("express");
const router = express.Router();
const sql = require("../mysql");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const login = require("../middleware/login");
const jwt = require("jsonwebtoken");


router.post("/cadastrar", jsonParser, login.obrigatorio, (req, res, next) => {
  // #swagger.tags = ['Solicitações']
  let errors = [];

  if (!req.body.tipo_solicitacao) {
    errors.push({ key: "tipo_solicitacao", value: "campo obrigatório" });
  }
  if (!req.body.endereco.rua) {
    errors.push({ key: "endereco.rua", value: "campo obrigatório" });
  }
  if (!req.body.endereco.numero) {
    errors.push({ key: "endereco.numero", value: "campo obrigatório" });
  }
  if (!req.body.endereco.bairro) {
    errors.push({ key: "endereco.bairro", value: "campo obrigatório" });
  }
  if (!req.body.data_agendamento) {
    errors.push({ key: "data_agendamento", value: "campo obrigatório" });
  }

  if (errors.length > 0) {
    res.status(400).send({
      status: 400,
      errors: errors,
      message: "Erro ao registrar nova solicitação. Tente mais tarde.",
      success: false,
    });
  } else {

    console.log(req.usuario)
    let querySelect = `SELECT id FROM usuario WHERE email = '${req.usuario.usuario}'`;
    sql.query(querySelect, (err, resDbS) => {

      if (err) {
        console.log("error: ", err);
        return;
      }
      console.log(resDbS);


      let query = `INSERT INTO solicitacoes (id_usuario, data_cadastro, tipo_solicitacao, data_agendamento, comentario, endereco) VALUES (${resDbS[0]['id']}, (SELECT CURRENT_DATE), ${req.body.tipo_solicitacao}, '${req.body.data_agendamento}', '', '${req.body.endereco.rua} ${req.body.endereco.numero} ${req.body.endereco.bairro}') ;`;

      sql.query(query, (err, resDb) => {
        if (err) {
          console.log("error: ", err);

          res.status(400).send({
            status: 400,
            errors: err,
            message: "Erro ao registrar nova solicitação. Tente mais tarde.",
            success: false,
          });
        } else {
          res.status(200).send({
            status: 200,
            message: "Solicitação criada com sucesso!",
            success: true,
            teste: req.body,
          });
        }
      });
    })
  }
});

router.delete("/cancelar/:id", login.obrigatorio, (req, res, next) => {
  // #swagger.tags = ['Solicitações']
  let query = `UPDATE solicitacoes SET flg_ativo = false WHERE cod_solicitacao = '${req.params.id}'`;

  sql.query(query, (err, resDb) => {
    if (err) {
      console.log("error: ", err);

      return res.status(400).send({
        status: 400,
        errors: err,
        message: "Erro ao consultar solicitação. Tente mais tarde.",
        success: false,
      });
    } else {
      return res.status(200).send({
        message: "Solicitação cancelada com sucesso!",
        status: 200,
        success: true,
      });
    }
  });
});

router.get("/user", login.obrigatorio, (req, res, next) => {
  // #swagger.tags = ['Solicitações']
  let querySelect = `SELECT id FROM usuario WHERE email = '${req.usuario.usuario}'`;
  sql.query(querySelect, (err, resDbS) => {

    if (err) {
      res.status(400).send({
        status: 400,
        errors: err,
        message: "Erro ao consultar as solicitações. Tente novamente mais tarde.",
        success: false
      });
    }
    console.log(resDbS);
    let query = `
      SELECT 
        s.cod_solicitacao,
        ts.titulo_solicitacao as tipo_solicitacao,
        s.data_agendamento,
        s.endereco
      FROM solicitacoes s
      INNER JOIN tipo_solicitacoes ts
        ON ts.id_solicitacao = s.tipo_solicitacao
      WHERE s.id_usuario = '${resDbS[0]['id']}' AND s.flg_ativo = TRUE;`;

    sql.query(query, (err, resDb) => {
      if (err) {
        console.log("error: ", err);

        res.status(400).send({
          status: 400,
          errors: err,
          message: "Erro ao consultar as solicitações. Tente novamente mais tarde.",
          success: false,
        });
      } else {
        res.status(200).send({
          model: resDb,
          message: "Consulta realizada com sucesso.",
          status: 200,
          success: true,
        });
      }
    });
  });
});

router.get("/tipo", jsonParser, (req, res, next) => {
  // #swagger.tags = ['Solicitações']

  let query = `select * from tipo_solicitacoes;`;

  sql.query(query, (err, resDb) => {
    if (err) {
      console.log("error: ", err);

      res.status(400).send({
        status: 400,
        errors: err,
        message: "Erro ao consultar solicitação. Tente mais tarde.",
        success: false,
      });
    } else {
      res.status(200).send({
        model: resDb,
        message: "Consulta realizada com sucesso.",
        status: 200,
        success: true,
      });
    }
  });
});

router.get("/tipo/:id", jsonParser, (req, res, next) => {
  // #swagger.tags = ['Solicitações']
  let query = `select * from tipo_solicitacoes WHERE id_solicitacao = ${req.params.id};`;

  sql.query(query, (err, resDb) => {
    if (err) {
      console.log("error: ", err);

      res.status(400).send({
        status: 400,
        errors: err,
        message: "Erro ao consultar solicitação. Tente mais tarde.",
        success: false,
      });
    } else {
      res.status(200).send({
        model: resDb,
        message: "Consulta realizada com sucesso.",
        status: 200,
        success: true,
      });
    }
  });
});

router.get("/informacoes_grafico", login.admin, (req, res, next) => {
  // #swagger.tags = ['Admin']
  let query = `
  select  distinct ts.titulo_solicitacao as tipo_lixo, count(s.tipo_solicitacao) as quantidade from solicitacoes s INNER JOIN
      tipo_solicitacoes ts
    ON s.tipo_solicitacao = ts.id_solicitacao
    group by s.tipo_solicitacao;`;

  sql.query(query, (err, resDb) => {
    if (err) {
      console.log("error: ", err);

      res.status(400).send({
        status: 400,
        errors: err,
        message: "Erro ao consultar solicitação. Tente mais tarde.",
        success: false,
      });
    } else {


      res.status(200).send({
        model: resDb,
        message: "Consulta realizada com sucesso.",
        status: 200,
        success: true,
      });
    }
  });
});

module.exports = router;
