const express = require('express');
const router = express.Router();
const sql = require('../mysql');
const md5 = require('md5');
const bodyParser = require("body-parser")
const jsonParser = bodyParser.json();
const jwt = require('jsonwebtoken');


router.post('/cadastrar_usuario', jsonParser, (req, res, next) => {
  let cpf = req.body.cpf || ""

  let query = `INSERT INTO usuario (nome,cpf,email,senha,dark_mode,flg_ativo, flg_admin) VALUES ('${req.body.nome}','${cpf}','${req.body.email}','${md5(req.body.senha)}', false, true, false)`;
  sql.query(query, (err, resDb) => {
    if (err) {
      console.log("error: ", err);
      return;
    }
    console.log(resDb);
    res.send({ status: 200, message: 'Usuário cadastrado com sucesso !', success: true }).status(200)
  });

  // #swagger.tags = ['Usuario']
});

router.post('/login', jsonParser, (req, res, next) => {
  let query = `SELECT * FROM usuario WHERE email = '${req.body.email}' AND senha = '${md5(req.body.senha)}'`
  sql.query(query, (err, resDb) => {
    if (err) {
      console.log("error: ", err);
      return;
    }
    console.log(resDb);

    if (resDb.length >= 1) {
      let token
      let admin = false
      if (resDb[0]['flg_admin']) {
        console.log('admin')
        token = jwt.sign({
          usuario: req.body.email,
          nome: resDb[0]['nome'],
          id: resDb[0]['id']
        }, `SeedAdminProvisoria`)

        admin = true
      } else {
        console.log('normal')
        token = jwt.sign({
          usuario: req.body.email,
          nome: resDb.nome,
          id: resDb.id
        }, `SeedProvisoria`)
      }

      res.send({ user:{nome: resDb[0]['nome'], email: resDb[0]['email']}, access_token: token, status: 200, flg_admin: admin, message: 'Usuario logado com sucesso', success: true }).status(200)

    }
    else {
      res.send({ status: 400, message: 'Usuario ou Senha incorretos', success: false }).status(400)
    }

  });
  // #swagger.tags = ['Usuario']
})





module.exports = router;
