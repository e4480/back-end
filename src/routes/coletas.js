const express = require("express");
const router = express.Router();
const sql = require("../mysql");

const { google } = require('googleapis');
const { OAuth2 } = google.auth

//const oAuth2Client = new OAuth2('521274477402-4982vucocerifkknn7s6bl0ud9clh2gj.apps.googleusercontent.com', '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')

//oAuth2Client.setCredentials({ refresh_token: '1//04rJqXXYvRKH1CgYIARAAGAQSNwF-L9IrJG9dmUI63yrnrCSrC5SXULqdhc7It_RozEHIIX8Ia0-ROtv9I_jHmFh42_7iUVn_r9c' })

//const calendar = google.calendar({ version: 'v3', auth: oAuth2Client })

router.get("/:bairro", (req, res, next) => {
  let query = `SELECT * FROM coleta c Where c.bairro = '${req.params.bairro}'`;

  sql.query(query, (err, resDb) => {
    if (err) {
      console.log("error: ", err);
      res.status(400).send({
        status: 400,
        errors: err,
        message: "Erro ao consultar os horários de coleta.",
        success: false,
      });
    } else {
      console.log(resDb);
      if(resDb == 0){
        res.send({status: 400, message: "Desculpe, nenhum registro foi encontrado para o bairro informado.", success: false }).status(400);
      }else{
      res.send({ model: resDb, status: 200, message: "Sucesso", success: true }).status(200);
      }
    }
  });
  // #swagger.tags = ['Coleta']
});

router.post("/lembrete", (req, res, next) => {
  let errors = []

  if (!req.body.data) {
    errors.push({ key: "data", value: "campo obrigatório" });
  }

  if (errors.length > 0) {
    res.send({ errors: errors, status: 400, message: "Erro ao criar lembrete no calendário.\nTente novamente mais tarde.", success: false }).status(400);
  } else {
    const event = {
      summary: 'Lembre Coleta de Lixo',
      location: 'Bairro',
      description: 'Coleta de lixo reciclável',
      start: {
        date: req.body.data,

      },
      end: {
        date: req.body.data,

      },
      ColorId: 1
    }

    calendar.events.insert(
      { calendarId: 'primary', resource: event },
      err => {
        if (err) {
          console.log(err)
          res.send({ errors: err.errors, status: 400, message: "Erro ao criar lembrete no calendário.\nTente novamente mais tarde.", success: false }).status(400);
        }
        res.send({ status: 200, message: "Lembrete criado com sucesso.", success: true }).status(200);
      }
    )
  }
  // #swagger.tags = ['Coleta']
});

module.exports = router;
